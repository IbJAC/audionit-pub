<?php
require_once($_SERVER['DOCUMENT_ROOT'] .'/scripts/config.php');

$timestamp = time();
$path = 'd/'. $_REQUEST['uptype'];
$format = 'mp4';
$inval = 'true';
$usefname = 'true';
$uniquefname = 'true';

$callback = $_SERVER['PHP_SELF'];

if ($_REQUEST['public_id'] != '') {
	$qsParams = explode('&', $_SERVER['QUERY_STRING']);
	foreach ($qsParams AS $valpair) {
		$vpArr = explode('=', $valpair);
		if ($vpArr[0] == 'public_id') {
			$vid = str_replace('%2F', ':', $vpArr[1]);
			}
		}

    if ($_REQUEST['uptype'] == 'a') {
        $vidSrc = '<source src="http://res.cloudinary.com/'. cloudNAME .'/video/upload/l_video:'. $vid .',so_5/d/a/demo.mp4">
               <source src="http://res.cloudinary.com/'. cloudNAME .'/video/upload/l_video:'. $vid .',so_5/d/a/demo.webm">
               <source src="http://res.cloudinary.com/'. cloudNAME .'/video/upload/l_video:'. $vid .',so_5/d/a/demo.ogv">';
        } else {
        $frameLen = $_REQUEST['ulen'] + 1;
        if ($_REQUEST['w'] < $_REQUEST['h']) {
            $x = 'x_345';
            } else if ($_REQUEST['w'] == '480') {
            $x = 'x_285';
            } else {
            $x = 'x_245';
            }
        $vidSrc = '<source src="http://res.cloudinary.com/'. cloudNAME .'/video/upload/du_5.1/l_video:d:v:frame,fl_splice,du_'. $frameLen .'/fl_layer_apply,so_5.2/l_video:'. $vid .','. $x .',y_140,h_193,so_5.2,du_'. $_REQUEST['ulen'] .'/l_video:d:v:end,fl_splice/d/v/start.mp4">';
        }
    }

if ($_REQUEST['reload'] == '0') {
    $vidAttrs = 'poster="/images/audionit-poster.png"';
    if ($_REQUEST['autoplay'] != '0') {
        $vidAttrs .= ' autoplay';
        }
    } else {
    $vidAttrs = 'poster="/images/audionit-loading.png"';
    }
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset='UTF-8' />
		<title>AudiOnIt - If you feel it...say it!</title>
		<meta name="description" content="AudiOnIt" />
		<meta name="author" content="AudiOnIt" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>

	<body onload="pgReload('<?=$_REQUEST['reload']?>');" style="margin:0; padding:0; background-color:#121212;">
		
        <!-- The Modal -->
        <video id="vidSec" <?=$vidAttrs?> width="100%" controls>
           <?=$vidSrc?>
           Your browser does not support the video tag.
        </video>
                        
        <script src="https://code.jquery.com/jquery.js"></script>
        <script>
        function pgReload (r) {
            if (r=='1') {
                //alert('reload');
                window.location.href = '<?=$callback?>?<?=str_replace('reload=1', 'reload=0', $_SERVER['QUERY_STRING'])?>';
            }
            /*var query = window.location.search.substring(1);
            var vars = query.split("&");
            if (query == '') { return null; }
            
            for (var i=0; i<vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == 'reload' && pair[1] == '1'){
                    window.location.href = '<?=$callback?>?'+ query.replace("reload=1", "reload=0");
                }
            }*/
        }
        </script>
    </body>
</html>
