$(document).ready(function() {
    $('#menu').click(function() {
        $('#menu .off, #menu .on').fadeToggle();
        $('#pelogo-sm').toggle('slide', {direction:'left'});
        });

    $('.expand-more').click(function() {
        $(this).parent().children('.expandable').css('height', 'auto');
        $(this).parent().children('.expand-less').show();
        $(this).hide();
        });
    
    $('.expand-less').click(function() {
        $(this).parent().children('.expandable').css('height', '3.5em');
        $(this).parent().children('.expand-more').show();
        $(this).hide();
        });
    })
