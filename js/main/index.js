var n = 2; // Viewable slider count
var p = 1; // Previous slider count
var carouselF = setInterval(carousel, 5000);

function carousel() {
    //alert(n +' '+ p)
    if ($(window).width() <= '730' && n == 2) { // Skip slider 2 on mobile
        n++;
        }

    $('#slide'+ n).fadeIn(1000).css('position', 'relative');
    $('#slide'+ p).fadeOut(1000).css('position', 'absolute');

    if (n == 2) {
        $('#commercialCTA').fadeIn(1000);
        } else {
        $('#commercialCTA').fadeOut(1000);
        }
    //alert($(window).width());
            
    n++;
    p++;
    if ($(window).width() <= '730' && p == 2) { // Skip slider 2 on mobile
        p++;
        }

    //alert(n);
    if (n > 3) {
        n = 1;
        } else if (p > 3) {
        p = 1;
        }
    //alert(n +' '+ p)
    }

function carouselclick() {
    clearInterval(carouselF);
    carousel();
    carouselF = setInterval(carousel, 5000);
    }
            
$(document).ready(function() {
    var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    //alert($('#pelogo').width());

    var video = document.getElementById('vid');
    video.addEventListener('click',function(){
        video.play();
        },false);
            
    $('#commercialCTA a, #commercialCTA-m').click(function() {
        $('#vidModal').show();
        })

    // Close Modal
    $('.close').on('click', function() {
        $(this).parent().hide();
        video.pause();
        })
            
    $('#hero').click(function() {
        carouselclick();
        });
    })