$(document).ready(function() { 
    var copy = [];
    copy['topsect'] = $('#topsect .expandable').height();
    copy['secsect'] = $('#secsect .expandable').height();
    var d = new Date();
    var s = d.getSeconds();
    var vid = document.getElementById("vidSec");
    
    $('#menu').click(function() {
        $('#menu .off, #menu .on').fadeToggle();
        $('#pelogo-sm').toggle('slide', {direction:'left'});
        });

    // Expand more/less
    $('.expandable').animate({"height": "3.5em"}, "fast");
    $('.expand-more').click(function() {
        //alert($(this).parents('section').attr('id'));
        //$(this).parent().children('.expandable').css('height', 'auto');
        $(this).parent().children('.expandable').animate({"height": copy[$(this).parents('section').attr('id')] +"px"}); 
        $(this).parent().children('.expand-less').show();
        $(this).hide();
        });
    
    $('.expand-less').click(function() {
        //$(this).parent().children('.expandable').css('height', '3.5em');
        $(this).parent().children('.expandable').animate({"height": "3.5em"}, "fast");
        $(this).parent().children('.expand-more').show();
        $(this).hide();
        });

    // Open Modal
    $('.demoClick').click(function() {
        if ($(this).attr('id') == 'audWrap' || $(this).attr('id') == 'audDemo') {
            $('#vidSec, #vidForm, #callInitMod').hide();
            $('#audForm, .modal-content').show();
            //if ($('#cFolder').val() == 'd/') {
            //    $('#cFolder').val('d/a/');
            //}
            } else if ($(this).attr('id') == 'vidWrap' || $(this).attr('id') == 'vidDemo') {
            $('#vidSec, #audForm, #callInitMod').hide();
            $('#vidForm, .modal-content').show();
            //if ($('#cFolder').val() == 'd/') {
            //    $('#cFolder').val('d/v/')
            //}
            }

        //alert($('#cFolder').val());
        $('#vidModal').show();
        })
    
    $('#audcard').click(function() {
        $('#aud3dModal').show();
        })

    $('#vidcard').click(function() {
        $('#vid3dModal').show();
        })

    // Close Modal
    $('.close').on('click', function() {
        $(this).parent().hide();
        $('#vidSec, #callInitMod').hide();
        $('#audForm, .modal-content').show();
        document.getElementById('vidSec').src = document.getElementById('vidSec').src +'&autoplay=0'
        document.getElementById("aud3d").load();
        document.getElementById("vid3d").load(); 
        })
    
    // Init Call
	$('#makeCall').click(function() {
        var request = $.ajax({
            url: "/scripts/placeOrder.php",
            type: "POST",
            dataType: "json",
            data: {
                inputCustomerPhone: $('#phone').val(),
                selectOrderLang: $('#lang').val(),
                demo: '1',
                inputProduct: 'Personal Expression Audio',
                inputCustomerName: 'there',
                inputRecipientName: 'Your Loved One',
                hiddenClientEmail: 'trash@audionit.com',
                hiddenClientName: 'Audio On It',
                hiddenClientDir: 'demo',
                inputOrderNum: $('#phone').val() +'-'+ s,
                hiddenClientID: '2',
                callWhen: 'now',
                inputCardType: 'Audio',
                toCloudinary: '1'
                }
            })
        .done(function(result) {
            if(result.error != null) {
                alert('An error has occurred. Please try again.');
                } else {
                $('#audForm, iframe, video').hide();
                $('#callInitMod').show();
                }
            })
        })
    })
