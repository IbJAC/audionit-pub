var gulp = require('gulp'),
	gutil = require('gulp-util'),
	uglify = require('gulp-uglify'),
	cleanCSS = require('gulp-clean-css'),
	concat = require('gulp-concat');

gulp.task('css', function () {
	gulp.src('css/stylesheets/*.css')
    .pipe(cleanCSS({compatibility: '*'}))
	.pipe(concat('all.min.css'))
    .pipe(gulp.dest('css/stylesheets'));
	});

gulp.task('index', function () {
	gulp.src('./js/main/index.js')
	.pipe(uglify())
	.pipe(concat('index.min.js'))
	.pipe(gulp.dest('./js/main'));
	});

gulp.task('cards', function () {
	gulp.src('./js/main/cards.js')
	.pipe(uglify())
	.pipe(concat('cards.min.js'))
	.pipe(gulp.dest('./js/main'));
	});

gulp.task('contact', function () {
	gulp.src('./js/main/contact.js')
	.pipe(uglify())
	.pipe(concat('contact.min.js'))
	.pipe(gulp.dest('./js/main'));
	});

gulp.task('default', function(){
    gulp.run('css');
    gulp.run('index');
    gulp.run('cards');
    gulp.run('contact');
});

//gulp.watch('./js/main/*', function () {
     //gulp.run('index');
     //gulp.run('cards');
//});