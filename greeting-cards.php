<?php
require_once($_SERVER['DOCUMENT_ROOT'] .'/scripts/config.php');

$timestamp = time();
$path = 'd/'. $_REQUEST['uptype'];
$format = 'mp4';
$inval = 'true';
$usefname = 'true';
$uniquefname = 'true';
$callback = $_SERVER['PHP_SELF'];
$public_id = urldecode($_REQUEST['public_id']);

// If audio/video has been uploaded
if ($public_id != '') {
    $style = '#vidModal, #vidSec { display:block; }
    .modal-content, iframe, video { display:none; }';

    // If uploaded to cloudinary, copy video to AOI server
    if ($_REQUEST['o'] > 0 && $_REQUEST['c'] > 0) {
        require_once($_SERVER['DOCUMENT_ROOT'] .'/scripts/config.php');
        require_once($_SERVER['DOCUMENT_ROOT'] .'/scripts/classes/ClientManageClss.php');

        $clientMng = new ClientManage();
        $clientInfo = $clientMng->selectClientLocations($_REQUEST['c']);
                
        $vidfile = 'http://res.cloudinary.com/'. cloudNAME .'/video/upload/'. $public_id .'.mp4';
        //$filename = str_replace('d/v/c/'. $_REQUEST['c'] .'/', '', $public_id);
        $filename = $_REQUEST['o'] .'_'. $_REQUEST['c'] .'_'. $clientInfo[0]['directory'] .'.mp4';
        $filepath = $_SERVER['DOCUMENT_ROOT'] .'/clients/'. $clientInfo[0]['directory'] .'/videO_files/'. $filename;
        //echo "$filename<br>".$_REQUEST['o']."<br>$filepath";
        if (!copy($vidfile, $filepath)) {
            $mssg = "Transfer error. Please reload the page.";
            $color = "background-color:#ff0000; color:#fff;";

            } else {
            $mssg = "Video received. Thank You!";
            $color = "background-color:#efefef; color:#3BA1D1;";
            }
        }
    
    // If customer came from text/email link
    } else if ($_REQUEST['o'] > 0 && $_REQUEST['c'] > 0) {
    $style = '#vidModal, #vidForm { display:block; }
    #audForm { display:none; }';
    $path = str_replace('d/'. $_REQUEST['uptype'], 'd/v/c/'. $_REQUEST['c'] .'/', $path);
    }

$style = "<style>$style $vidAutoPopup</style>";

$maxsize = '40M';
$maxsizebytes = str_replace('M', '000000', $maxsize);

$sha1 = 'folder='. $path .'&format='. $format .'&invalidate='. $inval .'&timestamp='. $timestamp .'&unique_filename='. $uniquefname .'&use_filename='. $usefname . apiSECRET;
$signature = sha1($sha1);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset='UTF-8' />
		<title>AudiOnIt - If you feel it...say it!</title>
		<meta name="description" content="AudiOnIt - purveyor of recordable audio and video cards for the gift and floral industries. Make your next gift card or flower delivery uniquely personal." />
		<meta name="author" content="AudiOnIt" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta property="og:image" content="http://audionit.com/images/card-vert.png">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="500">
        <meta property="og:image:height" content="372">
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/jquery.fileupload.css">
        <?php
        if (isset($_REQUEST['unmin'])) {
            echo '<link rel="stylesheet" type="text/css" href="css/stylesheets/main.css" />';
        } else {
            echo '<link rel="stylesheet" type="text/css" href="css/stylesheets/all.min.css" />';
        }
        ?>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css' />
		<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
        <?=$style?>
	</head>

	<body id="cardspg" style="background-color:#000000;">
        <div id="mainwrap">
		<header>
			<?php require('nav.php'); ?>
		</header>
		
		<main>
			<section id="topsect">
				<div>
                    <span id="audWrap" class="demoWrap">
                        <img id="audcard" title="Personal Expression Audio Greeting Card" src="/images/cards-video1.jpg" width="201" height="254" alt="Personal Expression Audio Greeting Card" />
                            <!--figure class="tryme">
                                <img title="Try Me..." src="/images/cards-tryMeBtn.png" width="126" height="40" alt="Try Me" />
                            </figure-->
                    </span>
                    <article>
                        <h1>Audio Cards</h1>
                        <span class="expandable">We offer two types of audio cards: one for you to sell to your customers in-person, and the other for sales online or over the phone. In-person, the message is recorded directly by speaking into a microphone on the card. Online and phone orders will get an automatic call back after purchase to let them record their message to our system. Then we'll let you know when a message is ready to be transferred onto a card to go out with the customer's order. Our Personal Expression greeting cards can be recorded, transferred and played in multiple languages, including English, Spanish, and more.<br>
                        <strong id="audDemo" class="demoClick">Click here for a demo...</strong></span>
                        <span class="expand-more">more &gt;</span>
                        <span class="expand-less">less &lt;</span>
                    </article>
				</div>
			</section>
			<section id="secsect">
				<div>
                    <span id="vidWrap" class="demoWrap">
                        <img id="vidcard" title="Personal Expression Video Greeting Card" src="/images/cards-video2.jpg" width="201" height="254" alt="Personal Expression Video Greeting Card" />
                            <!--figure class="tryme">
                                <img title="Try Me..." src="/images/cards-tryMeBtn.png" width="126" height="40" alt="Try Me" />
                            </figure-->
                    </span>
                    <article>
                        <h2>Video Cards</h2>
                        <span class="expandable">This is our newest generation greeting card with unlimited sales potential for you. After purchase, our system automatically sends out a link where the customer can upload their video. Whether captured right then from a cellphone, or as a full production edit off of a desktop or laptop computer, you can be assured nothing else comes close to the impression this card has when it reaches the recipient.<br>
                        <strong id="vidDemo" class="demoClick">Click here for a demo...</strong></span>
                        <span class="expand-more">more &gt;</span>
                        <span class="expand-less">less &lt;</span>
                    </article>
				</div>
			</section>
			
			<aside>
                <header>Preparing your card for delivery is quick and easy. After You've connected the card to your computer with the provided USB-cable, follow these simple steps:</header>
                <figure>
                    <span><img src="/images/one.png" width="30" height="40" title="Step 1" alt="Step 1" /></span>
                    <figcaption>Click the link in email or admin tool to open the Card Loader software.</figcaption>
                </figure>
                <figure>
                    <span><img src="/images/two.png" width="30" height="40" title="Step 2" alt="Step 2" /></span>
                    <figcaption>Click on the "Transfer" button in the dialog box.</figcaption>
                </figure>
                <figure>
                    <span><img src="/images/three.png" width="30" height="40" title="Step 3" alt="Step 3" /></span>
                    <figcaption>Wait for the transfer to finish. Your card is now ready.</figcaption>
                </figure>
                <figure><img src="/images/play-bttn.png" width="55" height="54" alt="icon" /></figure>
			</aside>
		</main>
		
		<footer>
			<div class="logo">
				<a href="/"><img src="/images/logo.png" width="263" height="73" title="AudiOnIt - If you feel it...say it!" alt="AudiOnIt - If you feel it...say it!" ></a>
				<div>Customer Support: 1-855-466-4382</div>
			</div>
			<div id="copyright">&copy; Personal Expression Cards, All Rights Reserved.</div>
		</footer>
        
        <!-- The Demo Modal -->
        <div id="vidModal" role="dialog" aria-labelledby="vidModal" data-backdrop="false">
            <span class="close" data-dismiss="modal" aria-label="Close">×</span>
            <div class="modal-content">
                <section id="audForm">
                    <header>Provide your phone number to receive our auto-call, follow the prompts, then return here after you've recorded your message.</header>
                    <form>
                        <fieldset>
                            <legend>DEMO</legend>
                            <div>
                                <label for="phone">Your Phone#:</label>
                                <input type="text" id="phone" name="inputCustomerPhone" placeholder="Numbers Only" value="" onBlur1="javascript:document.forms[0].elements['inputOrderNum'].value=this.value;" required="required" />
                            </div>
                            <div>
                                <input type="button" id="makeCall" value="Start Call" />
                            </div>
                        </fieldset>

                        <input type="hidden" name="selectOrderLang" id="selectOrderLang1" value="en-US" />
                        <input type="hidden" name="demo" value="1" />
                        <input type="hidden" name="inputProduct" value="Personal Expression Audio" />
                        <input type="hidden" name="inputCustomerName" value="there" />
                        <input type="hidden" name="inputRecipientName" value="Your Loved One" />
                        <input type="hidden" name="hiddenClientEmail" value="trash@audionit.com" />
                        <input type="hidden" name="hiddenClientName" value="Audio On It" />
                        <input type="hidden" name="hiddenClientDir" value="demo" />
                        <input type="hidden" name="inputOrderNum" value="" />
                        <input type="hidden" name="hiddenClientID" value="2" />
                        <input type="hidden" name="callWhen" value="now" />
                        <input type="hidden" name="toCloudinary" value="1" />
                    </form>
                </section>

                <section id="callInitMod" tabindex="-1">
                    <div role="document">
                        <header>Call in progress...</header>
                        <div>When you are done recording your message, click <input type="button" id="okBttn" value="Ok" data-dismiss="modal" /> to continue.</div>
                    </div>
                </section>
                
                <section id="vidForm">
                    <!--header style="text-align:center; padding:1em; font-size:3em;">Video card demo coming soon!</header-->
                    <header>Use the button below to upload <span class="no-desktop">or record </span> a video message and experience your own personalized video card.</header>
                    <form action="https://api.cloudinary.com/v1_1/<?=cloudNAME?>/video/upload" method="post" enctype="multipart/form-data" onSubmit="return showFileSize();">
                        <!-- The global progress bar -->
                        <div id="progress" class="progress">
                            <div class="progress-bar progress-bar-primary"></div>
                        </div>
                        <!-- The fileinput-button span is used to style the file input field as button -->
                        <div class="btn btn-primary fileinput-button">
                            <i class="glyphicon glyphicon-plus"></i>
                            <span>Select files...</span>
                            <!-- The file input field used as target for the file upload widget -->
                            <input id="fileupload" type="file" name="file" accept="video/*,video/mp4,audio/*" />
                        </div>
                        <!--span class="small">Max video file size: <?=$maxsize?></span><br /-->
                        <!-- The container for the uploaded files -->
                        <div id="files" class="files"></div>

                        <input id="cFolder" type="hidden" name="folder" value="<?=$path?>" />
                        <input type="hidden" name="format" value="<?=$format?>" />
                        <input type="hidden" name="invalidate" value="<?=$inval?>" />
                        <input type="hidden" name="timestamp" value="<?=$timestamp?>" />
                        <input type="hidden" name="unique_filename" value="<?=$uniquefname?>" />
                        <input type="hidden" name="use_filename" value="<?=$usefname?>" />
                        <input type="hidden" name="signature" value="<?=$signature?>"  />
                        <input type="hidden" name="api_key" value="<?=apiKEY?>" />
                    </form>
                    <em style="font-size:1.25em; color:#999;"><strong>(For demo purposes, use videos in lanscape mode no more than 20 seconds long.)</strong></em>
                </section>
                 
                <section id="loadMod" tabindex="-1" role="dialog" aria-labelledby="loadMod" data-backdrop="static">
                    <header>Proceeding to video...</header> 
                    <img src="/images/loadingcircle5.gif" width="150" height="80">
                </section>

            </div>	
            
            <div style="text-align:center; font-size:2em; padding:10px; font-weight:bold; <?=$color?>"><?=$mssg?></div>
            <iframe id="vidSec" src="http://audionit.com/vid-playback.php?<?=$_SERVER['QUERY_STRING']?>" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="height:403px; padding:0; box-shadow:none;"></iframe>
        </div>

        <!-- Video 3D Modal -->
        <div id="vid3dModal" role="dialog" aria-labelledby="vidModal" data-backdrop="false">
            <span class="close" data-dismiss="modal" aria-label="Close">×</span>
            <video id="vid3d" poster="/videoClips/introscreen-w.jpg" controls style="display:block; margin:auto; cursor:pointer;">
                <source src="/videoClips/video-3d.mp4">
            </video>
        </div>
    
        <!-- Audio 3D Modal -->
        <div id="aud3dModal" role="dialog" aria-labelledby="vidModal" data-backdrop="false">
            <span class="close" data-dismiss="modal" aria-label="Close">×</span>
            <video id="aud3d" poster="/videoClips/introscreen-w.jpg" controls style="display:block; margin:auto; cursor:pointer;">
                <source src="/videoClips/audio-3d.mp4">
            </video>
        </div>
        </div>
        
        <script src="https://code.jquery.com/jquery.js"></script>
        <script src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
        <script src="js/jquery.iframe-transport.js"></script>
        <!-- The basic File Upload plugin -->
        <script src="js/jquery.fileupload.js"></script>
        <!--script src="/js/jquery/jquery.html5video.js" type="text/javascript"></script-->
        <?php
        if (isset($_REQUEST['unmin'])) {
            echo '<script src="js/main/cards.js"></script>';
            } else {
            echo '<script src="js/main/cards.min.js"></script>';
            }
        ?>
        <script>
        $(document).ready(function() {
            // After audio has been recorded
            $('#okBttn').click(function () {
                $('#vidModal').hide();
                $('#audForm').show();
                $('#callInitMod').hide();

                window.location.href = '<?=$callback?>?public_id=<?=urlencode(str_replace('/a', '/', str_replace('/va/', '', $path)) .'a')?>%2F'+ $('#phone').val() +'-'+ s +'_2_demo&reload=1&uptype=a';
                })

            // Video Upload
            $(function () {
                'use strict';
                // Change this to the location of your server-side upload handler:
                var url = 'https://api.cloudinary.com/v1_1/<?=cloudNAME?>/video/upload';
                $('#fileupload').fileupload({
                    url: url,
                    dataType: 'json',
                    paramName: 'file',
                    done: function (e, data) {
                        //alert(data.result.duration);
                        window.location.href = '<?=$callback?>?public_id='+ encodeURIComponent(data.result.public_id) +'&c=<?=$_REQUEST['c']?>&o=<?=$_REQUEST['o']?>&uptype=v&reload=1&ulen='+ data.result.duration +'&w='+ data.result.width +'&h='+ data.result.height;
                        },
                    progressall: function (e, data) {
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        $('#progress .progress-bar').css('width', progress + '%');

                        if (progress == '100') {
                            $('#vidForm').hide();
                            $('#loadMod').show();
                            }
                        },
                    fail: function (e, data) {}
                    })
                    .prop('disabled', !$.support.fileInput)
                    .parent().addClass($.support.fileInput ? undefined : 'disabled');
                });
            })
        </script>
    </body>
</html>
