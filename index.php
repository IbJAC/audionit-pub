<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset='utf-8' />
		<title>AudiOnIt - If you feel it...say it!</title>
		<meta name="description" content="AudiOnIt" />
		<meta name="author" content="AudiOnIt" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
        <?php
        if (isset($_REQUEST['unmin'])) {
            echo '<link rel="stylesheet" type="text/css" href="css/stylesheets/main.css" />';
        } else {
            echo '<link rel="stylesheet" type="text/css" href="css/stylesheets/all.min.css" />';
        }
        ?>
        
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css' />
		<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>

	<body id="home" style="background-color:#000000;">
        <div id="mainwrap">
		<header>
			<?php require('nav.php'); ?>
			<section id="hero">
                <img id="slide1" src="/images/home-carousel-1.jpg" alt="Slide 1" />
                <img id="slide2" src="/images/home-carousel-2.jpg" alt="Slide 2" />
                <img id="slide3" src="/images/home-carousel-3.jpg" alt="Slide 3" />
                <div class="content">
                    <div class="logo">
                        <span><img id="pelogo" title="Personal Expression Audio and Video Greeting Cards" src="/images/PEwhiteLogo-lg.png" alt="Personal Expression Audio and Video Greeting Cards" /></span>
                    </div>
                    <div id="commercialCTA">
                        <a href="#"><img src="/videoClips/miss-you-already-cover.jpg" border="0" width="455" /></a>
                    </div>
                </div>
                <div id="commercialCTA-m" >
                    <a href="#"><img src="/videoClips/miss-you-already-cover.jpg" border="0" width="455" /></a>
                </div>
			</section>
		</header>
		
		<main>
			<section class="odd">
				<div class="hdr-icon-wrap">
					<h2>Recordable Greeting Cards</h2>
					<img id="recordable-icon" src="/images/icons/recordable-icon.png" title="Recordable Greeting Cards" alt="Recordable Greeting Cards" />
				</div>
				<article><h1 style="color:#000;">Create a new income stream with <strong>Personal Expression’s</strong> unique remote recordable personalized audio and video greeting cards.</h1> Our breakthrough technology lets your customers record and upload an audio or video message that you easily transfer to one of our <strong>Personal Expression</strong> cards and ship with their order.</article>
			</section>
			
			<section class="even">
				<div class="hdr-icon-wrap">
					<h2>Online Stores</h2>
					<img src="/images/icons/computer-icon.png" width="98" height="78" title="Brick-&amp;-Mortar" alt="Brick-&amp;-Mortar" />
				</div>
                <article>For online stores <strong>Personal Expression's</strong> revolutionary and simple technology enables you to offer your customers the perfect accompaniment to their gift purchase – a personalized, heartfelt audio or video greeting in the form of a card that you send as part of their order.</article>
			</section>
			
			<section class="odd">
				<div class="hdr-icon-wrap">
					<h2>Brick -&amp;- Mortar</h2> 
					<img src="/images/icons/brick-icon.png" width="103" height="99" title="Online Stores" alt="Online Stores" />
				</div>
                <article>In your brick-and-mortar gift-, florist- or card-shop, you can stock <strong>Personal Expression</strong> cards. This lets your customers record a greeting on the spot if you are delivering a gift. Or they can purchase a <strong>Personal Expression</strong> audio or video card from you, record when convenient and send or deliver themselves.</article>
			</section>
		</main>
		
		<footer>
			<div class="logo">
				<a href="/"><img src="/images/logo.png" width="263" height="73" title="AudiOnIt - If you feel it...say it!" alt="AudiOnIt - If you feel it...say it!" ></a>
				<div>Customer Support: 1-855-466-4382</div>
			</div>
			<div id="copyright">&copy; Personal Expression Cards, All Rights Reserved.</div>
		</footer>

        <!-- The Modal -->
        <div id="vidModal" role="dialog" aria-labelledby="vidModal" data-backdrop="false">
            <span class="close" data-dismiss="modal" aria-label="Close">×</span>
            <video id="vid" poster="/videoClips/miss-you-already-cover-w.png" controls style="display:block; margin:auto; cursor:pointer;">
                <source src="/videoClips/GoingOnATrip.mp4">
            </video>
        </div>
        </div>
        
        <script src="https://code.jquery.com/jquery.js"></script>
        <?php
        if (isset($_REQUEST['unmin'])) {
            echo '<script src="js/main/index.js"></script>';
            } else {
            echo '<script src="js/main/index.min.js"></script>';
            }
        ?>
    </body>
</html>
