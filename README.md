# README #

AudiOnIt's main public facing site.  
URL: [http://audionit.com](http://audionit.com)

### Tech Used ###

* HTML5
* CSS3
* PHP
* SASS
* Gulp

### 3rd Party API's, Services & Libraries  ###

* jQuery
* Google Fonts
* Bootstrap
* Cloudinary