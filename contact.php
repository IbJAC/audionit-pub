<?php
//echo phpinfo();
if ($_REQUEST['snd'] == '1' && $_SERVER["HTTP_REFERER"] == 'http://'. $_SERVER["HTTP_HOST"] . $_SERVER["PHP_SELF"]) {
    $to      = 'jerry@audionit.com';
    $subject = 'AOI Contact!';
    $message = "Name: ". $_REQUEST['name'] ."\n";
    $message .= "Phone: ". $_REQUEST['phone'] ."\n";
    $message .= $_REQUEST['message'];

    $headers = 'From: '. $_REQUEST['name'] .'<'. $_REQUEST['email'] .'>' ."\r\n" .
        'X-Mailer: PHP/' . phpversion();

    mail($to, $subject, $message, $headers);
    $reply = '<div id="mssgreply">Message Sent. Thank you!</div>';
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset='UTF-8' />
		<title>AudiOnIt - If you feel it...say it!</title>
		<meta name="description" content="AudiOnIt" />
		<meta name="author" content="AudiOnIt" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
        <?php
        if (isset($_REQUEST['unmin'])) {
            echo '<link rel="stylesheet" type="text/css" href="css/stylesheets/main.css" />';
        } else {
            echo '<link rel="stylesheet" type="text/css" href="css/stylesheets/all.min.css" />';
        }
        ?>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css' />
		<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
        <script src='https://www.google.com/recaptcha/api.js'></script>
	</head>

	<body id="contactpg" style="background-color:#000000;">
        <div id="mainwrap">
		<header>
			<?php require('nav.php'); ?>
		</header>
		
		<main>
            <form method="post" action="contact.php" action1="https://www.google.com/recaptcha/api/siteverify">
                <?=$reply?>
                <fieldset>
                    <legend>Information Request</legend>
                    <div>
                        <label for="name">Name:</label>
                        <input id="name" type="text" name="name" placeholder="required * " autofocus="autofocus" required="required" />
                    </div>
                    <div>
                        <label for="email">Email:</label>
                        <input id="email" type="text" name="email" placeholder="required * " required="required" />
                    </div>
                    <div>
                        <label for="phone">Phone:</label>
                        <input id="phone" type="text" name="phone" pattern="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" />
                    </div>
                    <div>
                        <label for="message">Message:</label>
                        <textarea id="message" name="message" placeholder="required * " required="required"></textarea>
                    </div>
                    <!--div class="g-recaptcha" data-sitekey="6LefICATAAAAAMBqJC7M7NnwULFE6Xk1NXdfw_L5" data-size="compact"></div-->
                    <div>
                        <label>&nbsp;</label>
                        <input type="reset" value="clear" /> <input type="submit" value="submit" />
                    </div>
                </fieldset>
                <input type="hidden" name="snd" value="1" />
            </form>
		</main>
		
		<footer>
			<div class="logo">
				<a href="/"><img src="/images/logo.png" width="263" height="73" title="AudiOnIt - If you feel it...say it!" alt="AudiOnIt - If you feel it...say it!" ></a>
				<div>Customer Support: 1-855-466-4382</div>
			</div>
			<div id="copyright">&copy; Personal Expression Cards, All Rights Reserved.</div>
		</footer>
        </div>
        
        <script src="https://code.jquery.com/jquery.js"></script>
        <script src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <?php
        if (isset($_REQUEST['unmin'])) {
            echo '<script src="js/main/contact.js"></script>';
            } else {
            echo '<script src="js/main/contact.min.js"></script>';
            }
        ?>
    </body>
</html>
